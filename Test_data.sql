-- Insert data into Addresses table
INSERT INTO Addresses (Address_ID, Address)
VALUES (1, 'Arenavägen 61'), (2, 'Arenavägen 62'), (3, 'Arenavägen 63');

-- Insert data into Users table
INSERT INTO Users (User_ID, Email, Password, Address_ID)
VALUES (1, 'user1@example.com', 'password1', 1), (2, 'user2@example.com', 'password2', 2), (3, 'user3@example.com', 'password3', 3);

-- Insert data into Roles table
INSERT INTO Roles (Role_ID, Role_Name)
VALUES (1, 'Admin'), (2, 'Manager'), (3, 'User');

-- Insert data into User_Roles table
INSERT INTO User_Roles (User_ID, Role_ID)
VALUES (1, 1), (2, 2), (3, 3);

-- Insert data into Groups table
INSERT INTO Group_s (Group_ID, Group_Name)
VALUES (1, 'Group 1'), (2, 'Group 2'), (3, 'Group 3');

-- Insert data into Group_Members table
INSERT INTO Group_Members (Group_ID, User_ID)
VALUES (1, 1), (1, 2), (2, 2), (3, 1), (3, 3);

-- Insert data into Messages table
INSERT INTO Messages (Message_ID, Group_ID, User_ID, Message_Content)
VALUES (1, 1, 1, 'Hello Group 1'), (2, 2, 2, 'Hello Group 2'), (3, 3, 3, 'Hello Group 3');

-- Insert data into Private_Messages table
INSERT INTO Private_Messages (Message_ID, Sender_ID, Receiver_ID, Message_Content)
VALUES (1, 1, 2, 'Hello user2'), (2, 2, 1, 'Hello user1'), (3, 3, 1, 'Hello user1');

-- Insert data into Products table
INSERT INTO Products (Product_ID, Product_Name, Inventory_Status)
VALUES ('P1', 'Product 1', 10), ('P2', 'Product 2', 5), ('P3', 'Product 3', 20);

-- Insert data into Stores table
INSERT INTO Stores (Store_ID, Store_Name)
VALUES (1, 'Store 1'), (2, 'Store 2'), (3, 'Store 3');

-- Insert data into Store_Products table
INSERT INTO Store_Products (Store_Product_ID, Store_ID, Product_ID)
VALUES (1, 1, 'P1'), (2, 2, 'P2'), (3, 3, 'P3');

-- Insert data into Orders table
INSERT INTO Orders (Order_ID, User_ID, Order_Status)
VALUES (1, 1, 'submitted'), (2, 2, 'packing'), (3, 3, 'ready to ship');

-- Insert data into Order_Lines table
INSERT INTO Order_Lines (Order_Line_ID, Order_ID, Product_ID, Quantity)
VALUES (1, 1, 'P1', 2), (2, 2, 'P2', 1), (3, 3, 'P3', 3);

