-- Create the Addresses table
CREATE TABLE Addresses (
  Address_ID INT NOT NULL AUTO_INCREMENT,
  Address VARCHAR(255) NOT NULL,
  PRIMARY KEY (Address_ID)
);

-- Create the Users table
CREATE TABLE Users (
  User_ID INT NOT NULL AUTO_INCREMENT,
  Email VARCHAR(255) NOT NULL,
  Password VARCHAR(255) NOT NULL,
  Address_ID INT,
  PRIMARY KEY (User_ID),
  FOREIGN KEY (Address_ID) REFERENCES Addresses(Address_ID)
);

-- Create the Roles table
CREATE TABLE Roles (
  Role_ID INT NOT NULL AUTO_INCREMENT,
  Role_Name VARCHAR(40) NOT NULL,
  PRIMARY KEY (Role_ID)
);

-- Create the User_Roles table
CREATE TABLE User_Roles (
  User_ID INT NOT NULL,
  Role_ID INT NOT NULL,
  PRIMARY KEY (User_ID, Role_ID),
  FOREIGN KEY (User_ID) REFERENCES Users(User_ID),
  FOREIGN KEY (Role_ID) REFERENCES Roles(Role_ID)
);

-- Create the Groups table
CREATE TABLE Group_s (
  Group_ID INT NOT NULL AUTO_INCREMENT,
  Group_Name VARCHAR(255) NOT NULL,
  PRIMARY KEY (Group_ID)
);

-- Create the Group_Members table
CREATE TABLE Group_Members (
  Group_ID INT NOT NULL,
  User_ID INT NOT NULL,
  PRIMARY KEY (Group_ID, User_ID),
  FOREIGN KEY (Group_ID) REFERENCES Group_s(Group_ID),
  FOREIGN KEY (User_ID) REFERENCES Users(User_ID)
);

-- Create the Messages table
CREATE TABLE Messages (
  Message_ID INT NOT NULL AUTO_INCREMENT,
  Group_ID INT NOT NULL,
  User_ID INT NOT NULL,
  Message_Content VARCHAR(255) NOT NULL,
  PRIMARY KEY (Message_ID),
  FOREIGN KEY (Group_ID) REFERENCES Group_s(Group_ID),
  FOREIGN KEY (User_ID) REFERENCES Users(User_ID)
);


-- Create the Private_Messages table
CREATE TABLE Private_Messages (
  Message_ID INT NOT NULL AUTO_INCREMENT,
  Sender_ID INT NOT NULL,
  Receiver_ID INT NOT NULL,
  Message_Content VARCHAR(255) NOT NULL,
  PRIMARY KEY (Message_ID),
  FOREIGN KEY (Sender_ID) REFERENCES Users(User_ID),
  FOREIGN KEY (Receiver_ID) REFERENCES Users(User_ID)
);

-- Create the Products table
CREATE TABLE Products (
  Product_ID VARCHAR(36) NOT NULL,
  Product_Name VARCHAR(255) NOT NULL,
  Inventory_Status INT NOT NULL,
  PRIMARY KEY (Product_ID)
);

-- Create the Stores table
CREATE TABLE Stores (
  Store_ID INT NOT NULL AUTO_INCREMENT,
  Store_Name VARCHAR(255) NOT NULL,
  PRIMARY KEY (Store_ID)
);

-- Create the Store_Products table
CREATE TABLE Store_Products (
  Store_Product_ID INT NOT NULL AUTO_INCREMENT,
  Store_ID INT NOT NULL,
  Product_ID VARCHAR(36) NOT NULL,
  PRIMARY KEY (Store_Product_ID),
  FOREIGN KEY (Store_ID) REFERENCES Stores(Store_ID),
  FOREIGN KEY (Product_ID) REFERENCES Products(Product_ID)
);

-- Create the Orders table
CREATE TABLE Orders (
  Order_ID INT NOT NULL AUTO_INCREMENT,
  User_ID INT NOT NULL,
  Order_Status ENUM('submitted', 'packing', 'ready to ship', 'shipped', 'cancelled') NOT NULL,
  PRIMARY KEY (Order_ID),
  FOREIGN KEY (User_ID) REFERENCES Users(User_ID)
);

-- Create the Order_Lines table
CREATE TABLE Order_Lines (
  Order_Line_ID INT NOT NULL AUTO_INCREMENT,
  Order_ID INT NOT NULL,
  Product_ID VARCHAR(36) NOT NULL,
  Quantity INT NOT NULL,
  PRIMARY KEY (Order_Line_ID),
  FOREIGN KEY (Order_ID) REFERENCES Orders(Order_ID)
  );
